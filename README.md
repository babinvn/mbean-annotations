# MBean Annotations

Base class for simple management beans. The example below uses Spring but the
library does not requires Spring.

Declare MBean interface:

```
import me.bvn.jmx.Description;

public interface CalculatorServiceMBean {
    @Description("Total count of calculations")
    long getNumCalculations();
    ...
}
```

Define implementing class:

```
import me.bvn.jmx.Description;
import me.bvn.jmx.MBeanName;
import me.bvn.jmx.SimpleMBean;

@Service("calculatorService")
@MBeanName("CalculatorLib:Type=CalculatorService")
@Description("Risk index calculator service")
public class CalculatorServiceImpl extends SimpleMBean implements CalculatorService, CalculatorServiceMBean {
  ...
  @PostConstruct
  public void register() {
      try {
          this.registerMBean();
      } catch (InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException ex) {
          LOGGER.warn("Failed to register calculator service management bean", ex);
      }
  }
  @PreDestroy
  public void unregister() {
      this.unregisterMBean();
  }
  ...
}
```

## Using with Scala

Declare MBean trait:

```
trait CalculatorServiceMBean {
  @Description("Total count of calculations")
  def getNumCalculations(): Long
  ...
}
```

Define implementing class:

```
@Service("calculatorService")
@MBeanName("CalculatorLib:Type=CalculatorService")
@Description("Risk index calculator service")
class CalculatorServiceImpl
  extends SimpleMBean(classOf[CalculatorServiceMBean])
    with CalculatorService
    with CalculatorServiceMBean {

  @PostConstruct
  def register(): Unit = {
    try {
      this.registerMBean()
    } catch {
      case ex @ (_: InstanceAlreadyExistsException |
                 _: MBeanRegistrationException |
                 _: NotCompliantMBeanException) =>
        CalculatorServiceImpl.LOGGER.warn("Failed to register calculator service management bean", ex)
    }
  }

  @PreDestroy
  def unregister(): Unit = this.unregisterMBean()
}
```
