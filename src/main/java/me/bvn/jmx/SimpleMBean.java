package me.bvn.jmx;

import java.lang.annotation.Annotation;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.StandardMBean;

public class SimpleMBean extends StandardMBean {
    final Class mbeanInterface;

    @Override
    protected String getDescription(MBeanInfo info) {
        if (info == null)
            return null;

        try {
            Class c = Class.forName(info.getClassName());
            Description desc = (Description)c.getAnnotation(Description.class);
            if (desc == null) {
                desc = (Description)mbeanInterface.getAnnotation(Description.class);
            }
            return desc.value();
        } catch(Exception ignore) {
            return info.getDescription();
        }
    }

    @Override
    @SuppressWarnings({"null", "ConstantConditions"})
    protected String getDescription(MBeanAttributeInfo info) {
        if (info == null)
            return null;

        Method method = null;
        do {
            try {
                method = mbeanInterface.getMethod("get" + info.getName());
                if (method != null)
                    break;
            } catch(NoSuchMethodException | SecurityException ignore) {
            }

            try {
                method = mbeanInterface.getMethod("is" + info.getName());
                if (method != null)
                    break;
            } catch(NoSuchMethodException | SecurityException ignore) {
            }

            try {
                method = mbeanInterface.getMethod("set" + info.getName());
            } catch(NoSuchMethodException | SecurityException ignore) {
            }
        } while(false);

        try {
            return method.getAnnotation(Description.class).value();
        } catch(Exception ignore) {
            return super.getDescription(info);
        }
    }

    @Override
    protected String getDescription(MBeanOperationInfo op) {
        if (op == null)
            return null;

        Method method = null;

        Method[] methods = mbeanInterface.getMethods();
        for (Method m : methods) {
            if (!m.getName().equals(op.getName()))
                continue;
            Class[] params = m.getParameterTypes();
            if (params.length != op.getSignature().length)
                continue;
            boolean found = true;
            for (int i = 0; i < params.length; i ++) {
                if (!params[i].getName().equals(((MBeanParameterInfo)op.getSignature()[i]).getType())) {
                    found = false;
                    break;
                }
            }
            if (found) {
                method = m;
                break;
            }
        }

        if (method == null) {
            return super.getDescription(op);
        }

        try {
            return method.getAnnotation(Description.class).value();
        } catch(Exception ignore) {
            return super.getDescription(op);
        }
    }

    @Override
    protected String getDescription(MBeanOperationInfo op, MBeanParameterInfo param, int sequence) {
        if (op == null || param == null)
            return null;

        Method method = null;

        Method[] methods = mbeanInterface.getMethods();
        for (Method m : methods) {
            if (!m.getName().equals(op.getName()))
                continue;

            Class[] params = m.getParameterTypes();
            if (params.length != op.getSignature().length)
                continue;
            boolean found = true;
            for (int i = 0; i < params.length; i ++) {
                if (!params[i].getName().equals(((MBeanParameterInfo)op.getSignature()[i]).getType())) {
                    found = false;
                    break;
                }
            }
            if (found) {
                method = m;
                break;
            }
        }

        if (method == null) {
            return super.getDescription(op, param, sequence);
        }

        for (Annotation a : method.getParameterAnnotations()[sequence]) {
            if (a instanceof Description) {
                return ((Description)a).value();
            }
        }

        return super.getDescription(op, param, sequence);
    }

    public SimpleMBean(Class mbeanInterface) throws NotCompliantMBeanException {
        super(mbeanInterface);
        this.mbeanInterface = mbeanInterface;
    }

    public static MBeanServer getMBeanServer() {
        return ManagementFactory.getPlatformMBeanServer();
    }

    public String getMBeanName() {
        MBeanName name = this.getClass().getAnnotation(MBeanName.class);
        return (name == null)? null: name.value();
    }

    public void registerMBean() throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {
        synchronized(this) {
            ObjectName objectName;
            try {
                String name = getMBeanName();
                if (name == null) {
                    throw new RuntimeException("Failed to get MBeanName for an object of type " + this.getClass().getName());
                } else {
                    objectName = new ObjectName(name);
                }
            } catch(MalformedObjectNameException ex) {
                throw new RuntimeException(ex);
            }
        
            getMBeanServer().registerMBean(this, objectName);
        }
    }

    public void unregisterMBean() {
        synchronized(this) {
            ObjectName objectName = null;
            try {
                objectName = new ObjectName(getMBeanName());
                getMBeanServer().unregisterMBean(objectName);
            } catch (InstanceNotFoundException | MBeanRegistrationException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.FINEST, "Failed to unregister MBean " + objectName, ex);
            } catch (MalformedObjectNameException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
